//=require ../third-party/libs.js




(function ( $ ) {
	'use strict';
	$ ( function () {
		
		// "Bootstrap Button Select"
		$ ( '.btn-select .dropdown-menu a' ).click ( function () {
			$ ( this ).parents ( '.btn-select' ).find ( '.btn-select-value' ).text ( $ ( this ).text () );
		} );
		
		$(".nano").nanoScroller();
		// var labelID;
		//
		// $('label.css-label').on('tap',(function() {
		// 	labelID = $(this).attr('for');
		// 	$('#'+labelID).prop("checked", !$('#'+labelID).prop("checked"));
		// 	$('#'+labelID).trigger('click');
		// }));

		// "Scroll to the top"
		$ ( '.scroll-top-wrapper' ).on ( 'click' , function () {
			var element = $ ( 'body' );
			var offset = element.offset ();
			var offsetTop = offset.top;
			$ ( 'html, body' ).animate ( { scrollTop : offsetTop } , 300 , 'linear' );
		} );


		// match the heights for groups of elements automatically
		$ ( '.footer-column,.feature-box' ).matchHeight ( {
			byRow : true ,
			property : 'height' ,
			target : null ,
			remove : false
		} );

		// enable all popovers
		$ ( '[data-toggle="popover"]' ).popover ( {
			html : true ,
			content : function () {
				return $ ( this ).data ( "content" ).html ();
			} ,
			title : function () {
				return $ ( this ).data ( "original-title" ).html ();
			}
		} );


		/** Megamenu */
		$ ( document ).on ( 'click' , '.mega-dropdown' , function ( e ) {
			e.stopPropagation ()
		} );
		
	
		if ( matchMedia ( 'only screen and (min-width: 993px)' ).matches ) {
			$ ( ".mega-dropdown > a" ).mouseenter (
				function () {
					$ ('.mega-dropdown').removeClass ( 'open' );
					$ ( this ).parent('.mega-dropdown').toggleClass ( 'open' );
				} );
			$( ".nav.navbar-left" )
				.mouseleave(function() {
					$ ('.mega-dropdown').removeClass ( 'open' );
				});
		}


		$ ( '.logo .menu-left' ).click ( function () {
			$ ( '#navbar-default' ).collapse ( 'toggle' );
		} );

		$('#user-connexion').on('show.bs.modal', function(e) {
			$(".e-commerce-container").offcanvas('hide');
			return e.preventDefault() ;
		});

		$ ( '#navbar-default' ).on ( 'shown.bs.collapse' , function ( e ) {
			if ( e.target !== this )
				return;
			$ ( ".logo .menu-left" ).removeClass ( "menu-close" ).addClass ( "menu-open" );
		} );

		$ ( '#navbar-default' ).on ( 'hidden.bs.collapse' , function ( e ) {
			if ( e.target !== this )
				return;
			$ ( ".logo .menu-left" ).removeClass ( "menu-open" ).addClass ( "menu-close" );
		} );
		
		
		function setModalMaxHeight(element) {
			var $element     = $(element);
			var $content     = $element.find('.modal-content');
			var borderWidth   = $content.outerHeight() - $content.innerHeight();
			var dialogMargin  = $(window).width() < 768 ? 20 : 60;
			var contentHeight = $(window).height() - (dialogMargin + borderWidth);
			var headerHeight  = $element.find('.modal-header').outerHeight() || 0;
			var footerHeight  = $element.find('.modal-footer').outerHeight() || 0;
			var maxHeight     = contentHeight - (headerHeight + footerHeight);

			$content.css({
				'overflow': 'hidden'
			});

			$element
				.find('.modal-body').css({
				'max-height': maxHeight,
				'overflow-y': 'auto'
			});
		}
		
		$('.modal').on('shown.bs.modal', function(e) {
			setModalMaxHeight(this);
		});
	

		$(window).resize(function() {
			if ($('.modal.in').length != 0) {
				setModalMaxHeight($('.modal.in'));
			}
		});
		
	} );
	
}) ( jQuery );

(function ( $ ) {
	$('.nano').nanoScroller({
		preventPageScrolling: true,
		iOSNativeScrolling: true ,
		sliderMaxHeight: 100,
		alwaysVisible: true
	});
	$( '.nano' ).on( 'mousewheel DOMMouseScroll', function ( e ) {
		var e0 = e.originalEvent,
			delta = e0.wheelDelta || -e0.detail;
		this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
		e.preventDefault();
	});
}) ( jQuery );