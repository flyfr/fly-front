/** Global */
//=require ../../vendors/jquery/dist/jquery.min.js
//=require ../../vendors/bootstrap/assets/javascripts/bootstrap.js
//=require ../../vendors/jasny-bootstrap/js/offcanvas.js
//=require ../../vendors/jasny-bootstrap/js/transition.js
//=require ../../vendors/matchheight/dist/jquery.matchHeight.js
//=require ./jquery.nanoscroller.js

/** Product & Catalogue Page */
//=require ../../vendors/slick-carousel/slick/slick.min.js

/** Product Page*/
//=require ../../vendors/ez-plus/src/jquery.ez-plus.js
//=require ../../vendors/perfect-scrollbar/js/perfect-scrollbar.jquery.js
//=require ../../vendors/jquery-geolocation/jquery.geolocation.js
//=require ../../vendors/maplace-js/dist/maplace.js

/** Home page + Page product*/
//=require ../../vendors/jquery-touchswipe/jquery.touchSwipe.js



var jQuery = jQuery.noConflict();


if ( typeof Prototype !== "undefined" ) {
	if (Prototype.BrowserFeatures.ElementExtensions) {
		var disablePrototypeJS = function (method, pluginsToDisable) {
				var handler = function (event) {
					event.target[method] = undefined;
					setTimeout(function () {
						delete event.target[method];
					}, 0);
				};
				pluginsToDisable.each(function (plugin) {
					jQuery(window).on(method + '.bs.' + plugin, handler);
				});
			},
			pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover'];
		disablePrototypeJS('show', pluginsToDisable);
		disablePrototypeJS('hide', pluginsToDisable);
		disablePrototypeJS('shown', pluginsToDisable);
		disablePrototypeJS('hidden', pluginsToDisable);
	}
}


var ui = (function ($) {
	return {
		renderProductsListe : function(){
			$ ( '.category-products .item-inner:first-child' ).each ( function () {
				if ( $ ( this ).parent ().hasClass ( 'rendered' ) )
					return true;
				$ ( this ).clone ().insertAfter ( this ).parent ().addClass ( 'rendered' );
				
			} );
			if(!$ ( '.category-products .item-inner:last-child .item-carousel' ).hasClass('slick-initialized')) {
				$ ( '.category-products .item-inner:last-child .item-carousel' ).slick ( {
					dots : false ,
					infinite : true ,
					speed : 300 ,
					slidesToShow : 4 ,
					slidesToScroll : 4
				} );
			}
		
			
			$ ( '.products-grid .item' ).matchHeight ( {
				byRow : true ,
				property : 'height' ,
				target : null ,
				remove : false
			} );
		},
		loanCalculator : function ($price , $period  ) {
		return Math.round10((parseFloat($price) / parseFloat($period)),-2);
	    }
	}
}(jQuery));


// Closure
(function() {
	/**
	 * Decimal adjustment of a number.
	 *
	 * @param {String}  type  The type of adjustment.
	 * @param {Number}  value The number.
	 * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
	 * @returns {Number} The adjusted value.
	 */
	function decimalAdjust(type, value, exp) {
		// If the exp is undefined or zero...
		if (typeof exp === 'undefined' || +exp === 0) {
			return Math[type](value);
		}
		value = +value;
		exp = +exp;
		// If the value is not a number or the exp is not an integer...
		if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
			return NaN;
		}
		// Shift
		value = value.toString().split('e');
		value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
		// Shift back
		value = value.toString().split('e');
		return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	}
	
	// Decimal round
	if (!Math.round10) {
		Math.round10 = function(value, exp) {
			return decimalAdjust('round', value, exp);
		};
	}
	// Decimal floor
	if (!Math.floor10) {
		Math.floor10 = function(value, exp) {
			return decimalAdjust('floor', value, exp);
		};
	}
	// Decimal ceil
	if (!Math.ceil10) {
		Math.ceil10 = function(value, exp) {
			return decimalAdjust('ceil', value, exp);
		};
	}
})();
