

(function ( $ , ui ) {
  'use strict';

  $ ( function () {

    $('.revue-de-presse .block-press .link').on("click",function(){
      $('.revue-de-presse .block-press').removeClass('active');
      $(this).parent('.block-press').addClass('active');
      $('.espace-presse .overlay').addClass('active');
    });
    $('.revue-de-presse .block-press .content .close').on("click",function(){
      $(this).parent().parent('.block-press').removeClass('active');
      $('.espace-presse .overlay').removeClass('active');
    });
    $('.espace-presse .overlay').on("click",function(){
      $(this).removeClass('active');
      $('.revue-de-presse .block-press').removeClass('active');
    });

  } );

}) ( jQuery , ui );

