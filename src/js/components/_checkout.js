

(function ( $ , ui ) {
	'use strict';
	
	$ ( function () {
		// show / mask input password
		$( "#showpass" ).on('click',function(){
			var input = document.getElementById('fly-password');
			if($(this).attr('data-click-state') == 0) {
				$(this).attr('data-click-state', 1);
				input.type = "password";
				$(this).html('Afficher');
			} else {
				$(this).attr('data-click-state', 0);
				input.type = "text";
				$(this).html('Masquer');
			}
			
		});
		$.fn.matchHeight._throttle = 80;
		$('.shipping-methods .shipping-options > .shipping-option ,.shipping-methods .shipping-options > .shipping-option .css-label').matchHeight({ property: 'min-height' });
		
		// virutal next button out of layout
		$ ( "form .form-submit.js" ).clone ().insertAfter ( ".cart-order" ).addClass ( 'rendered' );
		$ ( ".form-submit.js" ).on('click',function(){
			$("#"+$(this).data('form-id')).submit();
		});
		jQuery( ".shipping-options.address > li > a" ).each(function() {
			var content = jQuery(jQuery( this ).attr('href')).html();
			jQuery('<div/>', {
				class: 'mobile-rendered',
				html: content
			}).appendTo(this);
		});
		
		
		
		$ ( '#getPositionButton' ).on ( 'click' , function () {
			$.geolocation.get ( {
				win : function ( position ) {
					console.log ( "Your position is " + position.coords.latitude + ", " + position.coords.longitude );
				} , fail : function ( error ) {
					console.log ( "No location info available. Error code: " + error.code );
				}
			} );
		} );
		
		$ ( 'li.shipping-option a' ).on ( 'click' , function () {
			$('li.shipping-option a .css-radio').prop("checked", false);
			$(this).find('.css-radio').prop("checked", true);
		} );
		
		
		
		$ ( 'li.payment-option a' ).on ( 'click' , function () {
			$('li.payment-option a .css-radio').prop("checked", false);
			$(this).find('.css-radio').prop("checked", true);
		} );
		
		
		/**  modal Map */
		var LocsStores = [
			{
				lat : 45.9 ,
				lon : 10.9 ,
				title : 'Store A' ,
				html : [
					'&lt;h3&gt;Adresse C1&lt;/h3&gt;' ,
					'&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
				].join ( '' ) ,
				icon : 'http://128.199.60.141/img/common/POINTER_MAP.png' ,
				animation : google.maps.Animation.DROP ,
				show_infowindow : false
			} ,
			{
				lat : 44.8 ,
				lon : 1.7 ,
				title : 'Store B' ,
				html : [
					'&lt;h3&gt;Adresse C1&lt;/h3&gt;' ,
					'&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
				].join ( '' ) ,
				icon : 'http://128.199.60.141/img/common/POINTER_MAP.png' ,
				show_infowindow : false
			} ,
			{
				lat : 51.5 ,
				lon : - 1.1 ,
				title : 'Store C' ,
				html : [
					'&lt;h3&gt;Adresse C1&lt;/h3&gt;' ,
					'&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
				].join ( '' ) ,
				show_infowindow : false ,
				icon : 'http://128.199.60.141/img/common/POINTER_MAP.png' ,
			}
		];

		$ ( '#storelocator-checkout' ).on ( 'shown.bs.modal' , function () {
			var maplace = new Maplace ( {
				map_div : '#fl-sl-map-container-checkout' ,
				start : 1 ,
				locations : LocsStores ,
				generate_controls : false ,
				styles : {
					'Greyscale' : [ {
						featureType : 'all' ,
						stylers : [
							{ saturation : - 100 } ,
							{ gamma : 0.50 }
						]
					} ]
				} ,
				map_options : {
					zoom : 5
				}
			} ).Load ();
		} );
		
		/**  Selected Map in Tab */
		$('.shipping-option a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
			var maplace = new Maplace ( {
				map_div : '#fl-sl-map-container-checkout-selected' ,
				start : 1 ,
				locations : LocsStores ,
				generate_controls : false ,
				styles : {
					'Greyscale' : [ {
						featureType : 'all' ,
						stylers : [
							{ saturation : - 100 } ,
							{ gamma : 0.50 }
						]
					} ]
				} ,
				map_options : {
					zoom : 5
				}
			} ).Load ();
		})
		
		
	} );
	
}) ( jQuery , ui );

