/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */



(function ( $ , ui ) {
	'use strict';
	
	$ ( function () {
		
	
		
		var $carousel = $ ( '#products-show' );
		$carousel.carousel ();
		var handled = false;//global variable
		
		$carousel.swipe({
			
			swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
				
				if (direction == 'left') $(this).carousel('next');
				if (direction == 'right') $(this).carousel('prev');
				
			},
			allowPageScroll:"vertical"
			
		});
		// Zoom Product
		$carousel.bind ( 'slide.bs.carousel' , function ( e ) {
			var current = $ ( e.target ).find ( '.item.active' );
			var indx = $ ( current ).index ();
			if ( (indx + 2) > $ ( '.carousel-indicators li' ).length )
				indx = - 1;
			if ( ! handled ) {
				$ ( '.carousel-indicators li' ).removeClass ( 'active' );
				$ ( '.carousel-indicators li:nth-child(' + (indx + 2) + ')' ).addClass ( 'active' );
			}
			else {
				handled = ! handled;//if handled=true make it back to false to work normally.
			}
		} );
		
		$ ( ".carousel-indicators li" ).on ( 'click' , function () {
			//Click event for indicators
			$ ( this ).addClass ( 'active' ).siblings ().removeClass ( 'active' );
			//remove siblings active class and add it to current clicked item
			handled = true; //set global variable to true to identify whether indicator changing was handled or not.
		} );
		
		$ ( ".section-lookbook .btn-popover" ).each(function(i, obj) {
			$ ( this ).css({
				'top': $ ( this ).data("marker-top"),
				'left': $ ( this ).data("marker-left")
			});
		});
		
		
		$ ( '.btn-popover' ).on ( 'hidden.bs.popover' , function ( e ) {
			$ ( this ).removeClass("active");
		} );
		
		$ ( '.btn-popover' ).on ( 'shown.bs.popover' , function ( e ) {
			$ ( this ).addClass("active");
			if(!$( '.popover-content .item-carousel' ).hasClass('slick-initialized')) {
				$( '.popover-content .item-carousel' ).slick ( {
					dots : false ,
					infinite : true ,
					speed : 300 ,
					slidesToShow : 5 ,
					slidesToScroll : 5
				} );
			}
			
		} );
		
		if ( matchMedia ( 'only screen and (max-width: 993px)' ).matches ) {
			window.addEventListener("resize", function() {
				location.reload();
			}, false);
		}
		
	} );
	
}) ( jQuery , ui );