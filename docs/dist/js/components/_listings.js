/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */

/**
 * Created by amine on 10/28/2016.
 */



(function($,ui){
	'use strict';
	
	$ ( function () {
		
		
		
		
		// Reset filter button
		$ ( '#reset-filter' ).click ( function () {
			$ ( ".block-filter input[type=checkbox]" ).prop ( 'checked' , false );
		} );
		
		// display-product-option sidebar mobile
		$ ( ".display-product-option" ).clone ().insertAfter ( ".side-nav-categories" ).addClass ( 'rendered' );
		
		// sidebar mobile nav
		$ ( '.sidebar .side-nav-categories .block-title' ).click ( function () {
			var block_filter = $ ( '.side-nav-categories .category-menu' );
			if(block_filter.hasClass("in")){
				block_filter.collapse("hide");
			}else{
				block_filter.collapse("show");
			};
		} );
		
		
		// sidebar filter toggle
		$ ( '.show-filter-mobile' ).click ( function () {
			var block_filter = $ ( '.block-filter' );
			var icon_plus = $(this).find('.icon-plus');
			if(block_filter.hasClass("in")){
				icon_plus.html('+');
				block_filter.removeClass("in");
			}else{
				icon_plus.html('-');
				block_filter.addClass("in");
			}
		} );
		
		// Render Product ON hover
		ui.renderProductsListe();


	} );
})(jQuery,ui);