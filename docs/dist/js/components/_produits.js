/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */



(function ( $ , ui ) {
	'use strict';
	
	$ ( function () {
		
		$ ( '.btn-select .dropdown-menu .option' ).click ( function () {
			$ ( this ).parents ( '.btn-select' ).find ( '.btn-select-value' ).text ( $ ( this ).text () );
			$ ( this ).parents ( '.btn-select' ).find ( '.btn-select-input' ).val ( $ ( this ).data ( 'value' ) );
		} );
		
		$ ( '#period-terms' ).html (
			ui.loanCalculator (
				$ ( ".sale-price .price[content]" ).attr ( 'content' ) ,
				$ ( '#payment-period .btn-select .btn-select-input' ).val()
			)
		);
		
		$ ( '#payment-period .btn-select .dropdown-menu .option' ).click ( function () {
			var elem = $ ( this ).parents ( '.btn-select' ),
				label = elem.find ( '.btn-select-value' ),
				input = elem.find ( '.btn-select-input' );
			label.text ( $ ( this ).text () );
			input.val( $ ( this ).data ('value') );
			$ ( '#period-terms' ).html (
				ui.loanCalculator (
					$ ( ".sale-price .price[content]" ).attr ( 'content' ) ,
					$ ( this ).data ('value')
				)
			);
		} );
		
		$ ( '.add-to-basket .btn-select .dropdown-menu .option' ).click ( function () {
			var elem = $ ( this ).parents ( '.btn-select' ),
				label = elem.find ( '.btn-select-value' ),
				input = elem.find ( '.btn-select-input' );
				label.text ( $ ( this ).text () );
				input.val( $ ( this ).data ('value') );
		} );
		
		
		
		$ ( "input[name^='nation']" )
		
		if(!$ ( '.product-colors .item-carousel' ).hasClass('slick-initialized')) {
			$ ( '.product-colors .item-carousel' ).slick ( {
				dots : false ,
				infinite : true ,
				speed : 300 ,
				slidesToShow : 5 ,
				slidesToScroll : 5
			} );
		}
		
	
		
		
		$ ( '.products-grid .item' ).matchHeight ( {
			byRow : true ,
			property : 'height' ,
			target : null ,
			remove : false
		} );
		
		var $carousel = $ ( '#carousel-product' );
		$carousel.carousel ();
		var handled = false;//global variable
		
		$carousel.swipe({
			
			swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
				
				if (direction == 'left') $(this).carousel('next');
				if (direction == 'right') $(this).carousel('prev');
				
			},
			allowPageScroll:"vertical"
			
		});
		
	
		
		$carousel.bind ( 'slide.bs.carousel' , function ( e ) {
			var current = $ ( e.target ).find ( '.item.active' );
			var indx = $ ( current ).index ();
			if ( (indx + 2) > $ ( '.carousel-indicators li' ).length )
				indx = - 1;
			if ( ! handled ) {
				$ ( '.carousel-indicators li' ).removeClass ( 'active' );
				$ ( '.carousel-indicators li:nth-child(' + (indx + 2) + ')' ).addClass ( 'active' );
			}
			else {
				handled = ! handled;//if handled=true make it back to false to work normally.
			}
		} );
		
		$ ( ".carousel-indicators li" ).on ( 'click' , function () {
			//Click event for indicators
			$ ( this ).addClass ( 'active' ).siblings ().removeClass ( 'active' );
			//remove siblings active class and add it to current clicked item
			handled = true; //set global variable to true to identify whether indicator changing was handled or not.
		} );
		
		$ ( '#getPositionButton' ).on ( 'click' , function () {
			$.geolocation.get ( {
				win : function ( position ) {
					console.log ( "Your position is " + position.coords.latitude + ", " + position.coords.longitude );
				} , fail : function ( error ) {
					console.log ( "No location info available. Error code: " + error.code );
				}
			} );
		} );
		
		
		var LocsStores = [
			{
				lat : 45.9 ,
				lon : 10.9 ,
				title : 'Store A' ,
				html : [
					'&lt;h3&gt;Adresse C1&lt;/h3&gt;' ,
					'&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
				].join ( '' ) ,
				icon : 'http://128.199.60.141/img/common/POINTER_MAP.png' ,
				animation : google.maps.Animation.DROP ,
				show_infowindow : false
			} ,
			{
				lat : 44.8 ,
				lon : 1.7 ,
				title : 'Store B' ,
				html : [
					'&lt;h3&gt;Adresse C1&lt;/h3&gt;' ,
					'&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
				].join ( '' ) ,
				icon : 'http://128.199.60.141/img/common/POINTER_MAP.png' ,
				show_infowindow : false
			} ,
			{
				lat : 51.5 ,
				lon : - 1.1 ,
				title : 'Store C' ,
				html : [
					'&lt;h3&gt;Adresse C1&lt;/h3&gt;' ,
					'&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
				].join ( '' ) ,
				show_infowindow : false ,
				icon : 'http://128.199.60.141/img/common/POINTER_MAP.png' ,
			}
		];
		
		$ ( '#storelocator' ).on ( 'shown.bs.modal' , function () {
			var maplace = new Maplace ( {
				map_div : '#fl-sl-map-container' ,
				start : 1 ,
				locations : LocsStores ,
				generate_controls : false ,
				styles : {
					'Greyscale' : [ {
						featureType : 'all' ,
						stylers : [
							{ saturation : - 100 } ,
							{ gamma : 0.50 }
						]
					} ]
				} ,
				map_options : {
					zoom : 5
				}
			} ).Load ();
		} );
		
		// Render Product ON hover
		ui.renderProductsListe ();
	} );
	
}) ( jQuery , ui );