/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */

(function ( $ , ui ) {
  'use strict';

  $ ( function () {

    alert('balbalab');
    $('.accordion-job .container-acc .title').on("click", function () {

      if ($(this).parent().hasClass('active')) {
        $('.accordion-job .container-acc').removeClass('active');
        $(this).parent().removeClass('active');
      }
      else {
        $('.accordion-job .container-acc').removeClass('active');
        $(this).parent().addClass('active');
      }
    });

  });

}) ( jQuery , ui );
