/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */



(function ( $ , ui ) {
	'use strict';
	
	$ ( function () {
		
		$ ( 'li.credit-visu-option a' ).on ( 'click' , function () {
			$('li.credit-visu-option a .css-radio').prop("checked", false);
			$(this).find('.css-radio').prop("checked", true);
		} );

		$ ( 'div.credit-visu-option a' ).on ( 'click' , function () {
			$('div.credit-visu-option a .css-radio').prop("checked", false);
			$(this).find('.css-radio').prop("checked", true);
		} );
	} );
	
}) ( jQuery , ui );

