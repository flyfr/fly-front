/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */



(function ( $ , ui ) {
	'use strict';
	
	$ ( function () {
		
		$ ( '#period-terms' ).html (
			ui.loanCalculator (
				$ ( ".order .amount[content]" ).attr ( 'content' ) ,
				$ ( '#payment-period .btn-select .btn-select-input' ).val()
			)
		);
		
		$ ( '#payment-period .btn-select .dropdown-menu .option' ).click ( function () {
			var elem = $ ( this ).parents ( '.btn-select' ),
				label = elem.find ( '.btn-select-value' ),
				input = elem.find ( '.btn-select-input' );
			label.text ( $ ( this ).text () );
			input.val( $ ( this ).data ('value') );
			$ ( '#period-terms' ).html (
				ui.loanCalculator (
					$ ( ".order .amount[content]" ).attr ( 'content' ) ,
					$ ( this ).data ('value')
				)
			);
		} );
		
		
		// This button will increment the value
		$('.btn-number[data-type="plus"]').click(function(e){
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			var fieldName =  $('input[name=\"'+$(this).data('field')+'\"]');
			// Get its current value
			var currentVal = parseInt(fieldName.val());
			// If is not undefined
			if (!isNaN(currentVal)) {
				// Increment
				fieldName.val(currentVal + 1);
			} else {
				// Otherwise put a 0 there
				fieldName.val(0);
			}
		});
		// This button will decrement the value till 0
		$('.btn-number[data-type="minus"]').click(function(e) {
			// Stop acting like a button
			e.preventDefault();
			// Get the field name
			var fieldName =  $('input[name=\"'+$(this).data('field')+'\"]');
			// Get its current value
			var currentVal = parseInt(fieldName.val());
			// If it isn't undefined or its greater than 0
			if (!isNaN(currentVal) && currentVal > 0) {
				// Decrement one
				fieldName.val(currentVal - 1);
			} else {
				// Otherwise put a 0 there
				fieldName.val(0);
			}
		});
		
	} );
	
}) ( jQuery , ui );