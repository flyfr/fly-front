/*!
 * fly-front v1.0.1: Fly.fr frontend project
 * (c) 2017 Amine Mbarki
 * MIT License
 * https://bitbucket.org/flyfr/fly-front
 */

$( document ).ready(function() {
  //Player video: Show/hide function
  $(function(){
    //save the URL of the iframe so we can use it later
    var url = $('.video-container iframe').attr('src');
    $('#launch-video').click(function() {
      //Below we remove the URL to stop the video from playing, here we add it back in
      $('.video-container iframe').attr('src', url);
      $('.overlay-lightbox').addClass('visible');

    });
    $('.overlay-lightbox').click(function() {
      $(this).removeClass('visible');
      //Assign the iframe's src to null, which kills the video
      $('.video-container iframe').attr('src', '');
    });
  });
});
